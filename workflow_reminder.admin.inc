<?php

/**
 * @file
 * Workflow reminder Administration functionality.
 */

/**
 * Tha ajax callback called when workflow is changed.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The entire states section.
 */
function workflow_reminder_ajax_sid_callback($form, $form_state) {
  return $form['sid'];
}

/**
 * Gets the states of a given workflow.
 * 
 * @param string $wid
 *   The workflow id.
 * @return array
 *   Associative array of states ids and names of workflow states.
 */
function _workflow_reminder_ajax_get_sid_states($wid = 0) {
  $sids = array();

  if ($wid == 0) {
    $sids[0] = "Please select a valid workflow in the above drop down list";
  }
  else {
    $sids = _workflow_reminder_get_sid_set($wid);
  }

  return $sids;
}

/**
 * Form builder; Form to edit a given time.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The form with all the time fields.
 */
function workflow_reminder_form_time_edit($form, &$form_state) {

  $theFieldInformation = field_info_field('field_flujo_de_trabajo');

  $time = $form_state['time'];

  $form['title'] = array(
    '#type' => 'textfield',
    '#maxlength' => '255',
    '#title' => t('Title'),
    '#description' => t('A human-readable title for this option set.'),
    '#required' => TRUE,
    '#default_value' => $time->title,
  );
  $form['name'] = array(
    '#type' => 'machine_name',
    '#maxlength' => '255',
    '#machine_name' => array(
      'source' => array('title'),
      'exists' => 'workflow_reminder_time_exists',
    ),
    '#required' => TRUE,
    '#default_value' => $time->name,
  );



  // Get all the existing wids
  $wid_set = _workflow_reminder_get_wid_set();

  $form['wid'] = array(
    '#type' => 'select',
    '#title' => t('Workflow id'),
    '#multiple' => FALSE,
    '#description' => t('The id of the workflow'),
    '#options' => $wid_set,
    '#default_value' => $time->wid,
    // Bind an ajax callback to the change event (which is the default for the
    // select form type) of the first dropdown. It will replace the 'sid' select when rebuilt.
    '#ajax' => array(
      // When 'event' occurs, Drupal will perform an ajax request in the
      // background. Usually the default value is sufficient (eg. change for
      // select elements), but valid values include any jQuery event,
      // most notably 'mousedown', 'blur', and 'submit'.
      'event' => 'change',
      'callback' => 'workflow_reminder_ajax_sid_callback',
      'wrapper' => 'sid-replace',
    ),
  );

  $wid_selected = $form['wid']['#default_value'];
  if (!empty($form_state['values'])) {
    $wid_selected = $form_state['values']['wid'];
  }

  // When the form is rebuilt during ajax processing, the $sid_options variable
  // will now have the new value and so the options will change.    
  $sid_options = _workflow_reminder_ajax_get_sid_states($wid_selected);

  $form['sid'] = array(
    '#type' => 'select',
    '#title' => t('Workflow state id'),
    '#multiple' => FALSE,
    '#description' => t('The id of the workflow state'),
    '#options' => $sid_options,
    '#default_value' => $time->sid,
    // The entire enclosing div created here gets replaced when wid is changed.
    '#prefix' => '<div id="sid-replace">',
    '#suffix' => '</div>',
  );
  $form['maxtime'] = array(
    '#type' => 'textfield',
    '#title' => t('Maximum allowed time (hs)'),
    '#description' => t('The maximum time that this workflow can stay in this state, expressed in hours.'),
    '#size' => 30,
    '#required' => TRUE,
    '#default_value' => $time->maxtime,
  );
  $form['rfrom'] = array(
    '#type' => 'textfield',
    '#title' => t('From email'),
    '#description' => t('The sender email address.'),
    '#size' => 60,
    '#required' => TRUE,
    '#default_value' => isset($time->rfrom) ? $time->rfrom : variable_get('site_mail', ''),
  );
  $form['rlink'] = array(
    '#type' => 'textfield',
    '#title' => t('Link to node'),
    '#description' => t('The text of the link to the node.'),
    '#size' => 60,
    '#required' => TRUE,
    '#default_value' => isset($time->rlink) ? $time->rlink : 'Link to node',
  );
  $form['rmessage'] = array(
    '#type' => 'textfield',
    '#title' => t('Email message'),
    '#description' => t('The text of the email body message.'),
    '#size' => 60,
    '#required' => TRUE,
    '#default_value' => isset($time->rmessage) ? $time->rmessage : 'The node has been in the same state for too much time.',
  );
  $form['rsubject'] = array(
    '#type' => 'textfield',
    '#title' => t('Email subject'),
    '#description' => t('The text of the email subject.'),
    '#size' => 60,
    '#required' => TRUE,
    '#default_value' => isset($time->rsubject) ? $time->rsubject : 'Workflow reminder',
  );
  $form['rauthor'] = array(
    '#type' => 'checkbox',
    '#title' => t('Should the author be reminded?'),
    '#description' => t('Check if you want to remind the author.'),
    '#default_value' => $time->rauthor,
  );

  // Get all the existing roles 
  $role_set = _workflow_reminder_get_role_set();

  $form['rrole'] = array(
    '#type' => 'select',
    '#multiple' => TRUE,
    '#options' => $role_set,
    '#title' => t('Role/s of users to be reminded.'),
    '#description' => t('The users with this/these role/s will be reminded.'),
    '#required' => FALSE,
    '#default_value' => $time->rrole ? explode(',', $time->rrole) : NULL,
  );

  return $form;
}

/**
 * Global settings form builder.
 * 
 * @param array $form
 *    An associative array containing the structure of the form.
 * @param array $form_state
 *   A keyed array containing the current state of the form.
 * @return array
 *   The form with the global settings fields.
 */
function workflow_reminder_settings_form($form, $form_state) {
  // TODO: Think if this form is useful for something, otherwise remove it.
  $settings = variable_get('workflow_reminder_settings', array('checkbox' => TRUE)); //, 'label_prefix_value' => t('Workflow')));

  $form['settings'] = array(
    '#type' => 'container',
    '#tree' => TRUE,
  );

  $form['settings']['checkbox'] = array(
    '#type' => 'checkbox',
    '#title' => t('Check it?'),
    '#description' => t('Just a checkbox.'),
    '#default_value' => $settings['checkbox'],
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Save'),
  );

  return $form;
}

// Helpers

/**
 * Obtains the whole set of workflow ids.
 * 
 * @return array
 *   Associative array with ids of workflows (wid => label)
 */
function _workflow_reminder_get_wid_set() {
  $wid_result_set = db_query("select wid, label from {workflows}");

  $wid_set['0'] = t('Please select an wid');
  foreach ($wid_result_set as $record) {
    $wid_set["" . $record->wid] = $record->label;
  }

  return $wid_set;
}

/**
 * Obtains the set of states ids of a given workflow
 * 
 * @param $wid
 *   The workflow id
 * @return array
 *   Associative array with ids of states (sid => state)
 */
function _workflow_reminder_get_sid_set($wid) {
  $sid_result_set = db_query("select sid, state from {workflow_states} where wid = :wid", array(":wid" => $wid));
  $sid_set['0'] = t('Please select an state');
  foreach ($sid_result_set as $record) {
    $sid_set["" . $record->sid] = $record->state;
  }

  return $sid_set;
}

/**
 * Obtains the whole set of roles
 * 
 * @return array
 *   Associative array with ids of roles (rid => name)
 */
function _workflow_reminder_get_role_set() {
  $role_result_set = db_query("select rid, name from {role}");

  foreach ($role_result_set as $record) {
    $role_set["" . $record->rid] = $record->name;
  }
  return $role_set;
}