<?php

/**
 * Define this Export UI plugin.
 */
$plugin = array(
  'schema' => 'workflow_reminder', // As defined in hook_schema().
  'access' => 'administer workflow_reminder', // Define a permission users must have to access these pages.
  // Define the menu item.
  'menu' => array(
    'menu prefix' => 'admin/config/content',
    'menu item' => 'workflow_reminder',
    'menu title' => 'Workflow reminder',
    'menu description' => 'Administer Workflow reminder max allowed states times.',
  ),
  // Define user interface texts.
  'title singular' => t('time'),
  'title plural' => t('times'),
  'title singular proper' => t('Workflow reminder time'),
  'title plural proper' => t('Workflow reminder times'),
  // Define the names of the functions that provide the add/edit forms.
  'form' => array(
    'settings' => 'workflow_reminder_ctools_export_ui_form',
    'validate' => 'workflow_reminder_ctools_export_ui_form_validate',
    'submit' => 'workflow_reminder_ctools_export_ui_form_submit',
  ),
);

/**
 * Workflow reminder settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form. The arguments that drupal_get_form() was originally called with are available in the array $form_state['build_info']['args'].
 */
function workflow_reminder_ctools_export_ui_form(&$form, &$form_state) {
  // Load the admin form include
  module_load_include('inc', 'workflow_reminder', 'workflow_reminder.admin');

  // Make optionset reference in form_state
  $form_state['time'] = &$form_state['item'];

  // Load the configuration form
  $form = drupal_retrieve_form('workflow_reminder_form_time_edit', $form_state);
}

/**
 * Validation handler
 * 
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form. The arguments that drupal_get_form() was originally called with are available in the array $form_state['build_info']['args'].
 */
function workflow_reminder_ctools_export_ui_form_validate(&$form, &$form_state) {
  // Load every configured state
  $times = workflow_reminder_time_load_all();

  // Populate variables needed to validate
  $configured_states = array();
  $used_titles = array();
  $this_time = $form['name']['#value'];
  $this_title = strtolower($form['title']['#value']);
  foreach ($times as $configured_state) {
    if ($this_time != $configured_state->name) {
      $configured_states[] = $configured_state->sid;
      $used_titles[] = strtolower($configured_state->title);
    }
  }
 
  $state = $form['sid']['#value'];

  // A valid state must be selected
  if ($state == '0') {
    form_set_error('sid', t('Please select an item of field Workflow state id'));
  }

  // The same title can't be used more than once
  if (in_array($this_title, $used_titles)) {
    form_set_error('title', 'The title (' . $this_title . ') is already set in another configuration, please enter a new title.');
  }

  // The same state can't be configured more than once
  if (in_array($state, $configured_states)) {
    form_set_error('sid', 'The state (' . print_r($form['sid']['#options'][$state], true) . ') is already set in another configuration, please select a nonconfigured Workflow state id.');
  }
  
  // The maximum time value must be an integer greater than 0
  $max_time = $form['maxtime']['#value'];
  _workflow_reminder_validate_value_is_integer_positive($max_time, 'maxtime', $form);
}

/**
 * Submit callback for Custom Formatters settings form.
 *
 * @param $form
 *   Nested array of form elements that comprise the form.
 * @param $form_state
 *   A keyed array containing the current state of the form. The arguments that drupal_get_form() was originally called with are available in the array $form_state['build_info']['args'].
 */
function workflow_reminder_ctools_export_ui_form_submit(&$form, &$form_state) {
  // Convert string to array
  if (array_key_exists('rrole', $form_state['values'])) {
    $form_state['values']['rrole'] = implode(',', $form_state['values']['rrole']);
  }
}



// Helpers

/**
 * Validate a value is an integer greater than 0
 * 
 * @param $value
 *   The field value entered by the user.
 * @param $field_name
 *   The name of the field to be validated.
 * @param $form
 *   Nested array of form elements that comprise the form.
 */
function _workflow_reminder_validate_value_is_integer_positive($value, $field_name, $form) {
  if ($value !== '' && (!is_numeric($value) || intval($value) != $value || $value < 1)) {
    form_set_error($field_name, $form[$field_name]['#title'] . ' must be a positive integer.');
  }
}